@extends('layouts.main')


@section('content')
<section class="section-space bg-white" id="#cart">
    <div class="container">
        <div class="col-sm-12 col-xl-6 xl-100 row d-flex justify-content-center">
            <div class="card col-12">
                <div class="card-header">
                    <h5>Cart</h5>
                    @if ($message = Session::get('success'))
                    <div class="p-4 mb-3 bg-green-400 rounded">
                        <p class="text-green-800">{{ $message }}</p>
                    </div>
                    @endif
                </div>
                <div class="card-block p-2 row">
                    <div class="col-sm-12 col-lg-12 col-xl-12">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">
                                            <span class="lg:hidden" title="Quantity">Qtd</span>
                                            <span class="hidden lg:inline">Quantity</span>
                                        </th>
                                        <th class="hidden text-right md:table-cell">Price</th>
                                        <th class="hidden text-right md:table-cell">Remove </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cartItems as $item)
                                    <tr>
                                        <td class="hidden pb-4 md:table-cell">
                                            <a href="#">
                                                <img src="{{ $item->attributes->image }}" class="rounded"
                                                    alt="Thumbnail" style="max-width: 7rem">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                <p class="mb-2 md:ml-4">{{ $item->name }}</p>

                                            </a>
                                        </td>
                                        <td class="justify-center mt-6 md:justify-end md:flex">
                                            <div class="h-10 w-28">
                                                <div class="relative flex flex-row w-full h-8">

                                                    <form action="{{ route('cart.update') }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{ $item->id}}">
                                                        <div class="form-group row d-flex justify-content-center">
                                                            <input type="number" name="quantity"
                                                                value="{{ $item->quantity }}" class="form-control col-2"/>
                                                            <button type="submit" class="btn btn-info col-3 ml-2">update</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="hidden text-right md:table-cell">
                                            <span class="text-sm font-medium lg:text-base">
                                                Rp. {{ number_format($item->price) }}
                                            </span>
                                        </td>
                                        <td class="hidden text-right md:table-cell">
                                            <form action="{{ route('cart.remove') }}" method="POST">
                                                @csrf
                                                <input type="hidden" value="{{ $item->id }}" name="id">
                                                <button class="btn btn-info">x</button>
                                            </form>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-between">
                    <h4>Total: Rp. {{ number_format(Cart::getTotal()) }}</h4>
                    @if (count($cartItems) > 0)
                        <a href="#checkout" class="btn btn-info" data-toggle="collapse" data-target="#checkout">Checkout</a>
                    @endif
                    <form action="{{ route('cart.clear') }}" method="POST">
                        @csrf
                        <button class="btn btn-danger">Remove All Cart</button>
                    </form>
                </div>
            </div>
            @include('checkout')
        </div>
    </div>
</section>
@endsection
