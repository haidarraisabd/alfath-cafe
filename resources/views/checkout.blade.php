<div class="card col-6 collapse" id="checkout">
    <div class="card-header">
        <h5>Checkout</h5>
    </div>
    <div class="card-body">
        <form action="{{ route('checkout.store') }}" method="POST" class="w-100 row">
            @csrf
            <input type="text" name="user_id" id="user_id" value="{{Auth::id()}}" hidden>
            <input type="text" name="subtotal" id="subtotal" value="{{Cart::getTotal()}}" hidden>
            <div class="form-group col-12">
                <label for="nama">Atas Nama</label>
                <input type="text" class="form-control" name="nama" id="nama"
                    value="{{Auth::user()->name}}">
            </div>
            <div class="form-group col-12">
                <label for="no_meja">No. Meja</label>
                <input type="number" class="form-control" name="no_meja" id="no_meja">
            </div>
            <div class="form-group col-12">
                <label for="is_cash">Metode Pembayaran</label>
                <select name="is_cash" id="is_cash" class="form-control">
                    <option value="1">Cash</option>
                    <option value="0">Debit dan metode pembayaran lain</option>
                </select>
            </div>
            <div class="form-group col-12">
                <label for="nominal">Nominal</label>
                <input type="number" class="form-control" name="nominal" id="nominal">
            </div>
        </div>
        <div class="card-footer d-flex justify-content-end">
            <button type="submit" class="btn btn-danger">Checkout</button>
        </div>
    </form>
</div>
