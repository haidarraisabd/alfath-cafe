<div class="landing-home">
    <div class="landing-header">
        <div class="container-fluid" style="padding: 0; margin:0;">
            <nav class="navbar navbar-expand-lg fixed-top bg-white">
                <a class="navbar-brand" href="#"><img src="{{asset('assets/images/logo-alfath.png')}}" class="img-fluid" alt=""/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="navbar-toggler-icon">
                    <i class="fa fa-bars"></i>
                </span>
                </button>
                <div class="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cart.list')}}" class="btn bg-transparent"><i class="fa fa-shopping-bag"></i> Cart <span class="badge badge-dark">{{ Cart::getTotalQuantity()}}</span></a>
                        </li>
                        <li class="nav-item onhover-dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }}
                            </a>
                            <ul class="profile-dropdown onhover-show-div p-20">
                                <li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
    <div class="landing-body">
        <div class="container-fluid" style="margin-top: 200px;">
            <div class="row">
                <div class="col-lg-4 offset-lg-2 dashboard-image-pt-xs offset-md-1 col-md-5 offset-sm-0">
                    <div class="landing-left bg-grey">
                        <div>
                            <h1><span class="txt-danger">Alfath</span> Cafe and Working Space</h1>
                            <h5> <span class="digits txt-primary f-w-700 f-26">30+ </span> menus ,<span class="digits txt-info f-w-700 f-26"> 20+</span> drink , <span class="digits txt-danger f-w-700 f-26">  10+ </span> beverages</h5>
                            <p class="pr-5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Non nulla architecto molestiae accusantium libero qui, voluptas illum tempore fugit rem, laboriosam impedit eaque. Magni, eveniet eligendi optio ducimus incidunt amet?</p>
                            <div class="button-bottom">
                                <a href="{{ route('home')}}/#menus" class="btn btn-primary mr-2">Menus</a>
                                <a href="#rateus" class="btn btn-outline-primary-2x">Review Our Service</a>
                                <br>
                                <a href="{{ route('cart.list')}}/#cart" class="btn btn-secondary mt-2"><i class="fa fa-shopping-bag"></i> Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pr-0 col-md-6">
                    <img src="#" class="img-fluid" alt=""/>
                </div>
            </div>
        </div>
    </div>
</div>
