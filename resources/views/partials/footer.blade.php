<!--footer start-->
<section class="section-space footer-bg" id="rateus">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>if you like our products Please Rate Us</h2>
                <div class="star-rate">
                    <i class="fa fa-star font-danger"></i>
                    <i class="fa fa-star font-danger"></i>
                    <i class="fa fa-star font-danger"></i>
                    <i class="fa fa-star font-danger"></i>
                    <i class="fa fa-star font-danger"></i>
                </div>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCenter">
                    Rate Us
                </button>
                <style>
                    div.stars {
                        width: 270px;
                        display: inline-block
                    }

                    input.star {
                        display: none
                    }

                    label.star {
                        float: right;
                        padding: 10px;
                        font-size: 36px;
                        color: #4A148C;
                        transition: all .2s
                    }

                    input.star:checked~label.star:before {
                        content: '\f005';
                        color: #FD4;
                        transition: all .25s
                    }

                    input.star-5:checked~label.star:before {
                        color: #FE7;
                        text-shadow: 0 0 20px #952
                    }

                    input.star-1:checked~label.star:before {
                        color: #F62
                    }

                    label.star:hover {
                        transform: rotate(-15deg) scale(1.3)
                    }

                    label.star:before {
                        content: '\f006';
                        font-family: FontAwesome
                    }
                </style>
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalCenter" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Review Us</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- star rating -->
                                <div class="rating-wrapper">
                                    <div class="container d-flex justify-content-center">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="stars">
                                                    <form action="{{ route('review.store') }}" method="POST">
                                                        @csrf
                                                        <input class="star star-5" id="star-5" value="5" type="radio" name="star" />
                                                        <label class="star star-5" for="star-5"></label>
                                                        <input class="star star-4" id="star-4" value="4" type="radio" name="star" />
                                                        <label class="star star-4" for="star-4"></label>
                                                        <input class="star star-3" id="star-3" value="3" type="radio" name="star" />
                                                        <label class="star star-3" for="star-3"></label>
                                                        <input class="star star-2" id="star-2" value="2" type="radio" name="star" />
                                                        <label class="star star-2" for="star-2"></label>
                                                        <input class="star star-1" id="star-1" value="1" type="radio" name="star" />
                                                        <label class="star star-1" for="star-1"></label>
                                                        <input type="text" value="{{Auth::id()}}" name="user_id" hidden>
                                                        <div class="form-group">
                                                            <label for="comment">Komentar : </label>
                                                            <textarea name="comment" id="comment" class="form-control" rows="10"></textarea>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--footer ends-->
