<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
        content="universal admin is super flexible, powerful, clean & modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords"
        content="admin template, universal admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{asset('assets/images/logo-alfath.png')}}" type="image/x-icon')}}" />
    <link rel="shortcut icon" href="{{asset('assets/images/logo-alfath.png')}}" type="image/x-icon')}}" />
    <title>Alfat Cafe | Pelayanan</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome.css')}}">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/themify.css')}}">

    <!-- Flag icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/flag-icon.css')}}">

    <!-- Owl css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/owlcarousel.css')}}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.css')}}">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">

</head>

<body>
    <!-- Loader starts -->
    <div class="loader-wrapper">
        <div class="loader bg-white">
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
            <div class="line"></div>
        </div>
    </div>
    <!-- Loader ends -->

    <!--page-wrapper Start-->
    <div class="page-wrapper">
        <!--Page Body Start-->
        <div class="page-body-wrapper landing-main">
            @if (Auth::check())
            @include('partials.header')
            @endif

            @yield('content')

            @if (Auth::check())
            @include('partials.footer')
            @endif

            <!-- Tap on Top -->
            <div class="tap-top">
                <div>
                    <i class="fa fa-angle-double-up"></i>
                </div>
            </div>
            <!-- Tap on Ends -->
            <!--Page Body Ends-->
        </div>
        <!--Page Body Ends-->
        <!-- modal start -->
        <div class="modal fade" tabindex="-1" role="dialog" id="detailModal">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="card">
                            <div class="row product-page-main">
                                <div class="col-xl-4">
                                    <div id="sync1" class="product-slider owl-carousel owl-theme">
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/01.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/02.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/03.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/04.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/05.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/06.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/07.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/08.jpg')}}" alt="">
                                        </div>
                                    </div>
                                    <div id="sync2" class="owl-carousel owl-theme">
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/01.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/02.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/03.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/04.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/05.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/06.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/07.jpg')}}" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="{{asset('assets/images/ecommerce/product/08.jpg')}}" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-8">
                                    <div class="product-page-details">
                                        <h5>Fusion white & blue printed regular fit asymmetric</h5>
                                        <div class="d-flex">
                                            <select id="u-rating-fontawesome" name="rating" autocomplete="off">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            <span>(250 review)</span>
                                        </div>
                                    </div>
                                    <hr />
                                    <p> It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using Lorem Ipsum is
                                        that it has a more-or-less normal distribution of letters,It is a long
                                        established fact that a reader will be distracted by the readable content of a
                                        page when looking at its layout. The point of using Lorem Ipsum is that it has a
                                        more-or-less normal distribution of letters </p>
                                    <div class="product-price digits">
                                        <del>$350.00 </del>$26.00
                                    </div>
                                    <hr />
                                    <div>
                                        <table width="80%">
                                            <tbody>

                                                <tr>
                                                    <td>Brand :</td>
                                                    <td>shopcart_fashion</td>
                                                </tr>
                                                <tr>
                                                    <td>Availability :</td>
                                                    <td class="in-stock">In stock</td>
                                                    <td class="out-of-stock" style="display: none;">Out Of stock</td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <hr />
                                    <ul class="product-color m-t-15">
                                        <li class="bg-primary"></li>
                                        <li class="bg-secondary"></li>
                                        <li class="bg-success"></li>
                                        <li class="bg-info"></li>
                                        <li class="bg-warning"></li>
                                    </ul>
                                    <div class="m-t-15">
                                        <button type="button" class="btn btn-primary-gradien m-r-10"
                                            data-original-title="btn btn-info-gradien" title="">Add To Cart</button>
                                        <button type="button" class="btn btn-secondary-gradien m-r-10"
                                            data-original-title="btn btn-info-gradien" title="">Quick View</button>
                                        <button type="button" class="btn btn-success-gradien"
                                            data-original-title="btn btn-info-gradien" title="">Buy Now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- modal end -->
    </div>
    <!--page-wrapper Ends-->
    <!-- latest jquery-->
    <script src="{{asset('assets/js/jquery-3.2.1.min.js')}}"></script>

    <!-- Bootstrap js-->
    <script src="{{asset('assets/js/bootstrap/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap/bootstrap.js')}}"></script>

    <!-- owlcarousel js-->
    <script src="{{asset('assets/js/owlcarousel/owl.carousel.js')}}"></script>

    <!-- Sidebar jquery-->
    <script src="{{asset('assets/js/sidebar-menu.js')}}"></script>

    <!-- Theme js-->
    <script src="{{asset('assets/js/script.js')}}"></script>
    @yield('js')
    <script>
        "use strict";
    $(document).ready(function(){
        $("nav a").on('click', function(event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 0
                }, 1000, function(){
                });
                return false;
            }
        });
        /*----------------------------------------
         GO to Home
         ----------------------------------------*/
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 500) {
                $('.tap-top').fadeIn();
            } else {
                $('.tap-top').fadeOut();
            }
        });
        $('.tap-top').on('click', function() {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });

    });

    </script>
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    @include('sweetalert::alert')
</body>

</html>
