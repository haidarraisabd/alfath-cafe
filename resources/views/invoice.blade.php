@extends('layouts.main')
@section('content')
<div class="container mt-4">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice">
                        <div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="media">
                                            <div class="media-left">
                                                <img src="{{asset('assets/images/logo-alfath.png')}}" class="media-object img-60" alt="">
                                            </div>
                                            <div class="media-body m-l-20">
                                                <h4 class="media-heading">Alfath Cafe</h4>
                                                <p>hello@alfathcafe.com<br>
                                                    <span class="digits">(62) 812-6296-4751</span>
                                                </p>
                                            </div>
                                        </div>
                                        <!--End Info-->
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="text-md-right">
                                            <h3>Invoice #<span class="digits counter">{{$transaction->id}}</span></h3>
                                            <p>Issued: {{$transaction->created_at}}</span><br>
                                                Payment Due: {{$transaction->created_at}}</span>
                                            </p>
                                        </div><!--End Title-->
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <!--End InvoiceTop-->
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="media">
                                        <div class="media-left">
                                            <img src="https://img.icons8.com/external-inipagistudio-mixed-inipagistudio/64/000000/external-person-self-improvement-inipagistudio-mixed-inipagistudio.png"/>
                                        </div>
                                        <div class="media-body m-l-20">
                                            <h4 class="media-heading">{{$transaction->user->name}}</h4>
                                            <p>{{$transaction->user->email}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div id="project" class="text-md-right">
                                        <h6>Terimakasih telah memesan</h6>
                                        <p>Mohon ditunggu pesanan akan segera diproses</p>
                                    </div>
                                </div>
                            </div>
                            <!--End Invoice Mid-->

                            <div class="mt-2">
                                <div id="table" class="table-responsive invoice-table">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">
                                                    <span class="lg:hidden" title="Quantity">Qtd</span>
                                                    <span class="hidden lg:inline">Quantity</span>
                                                </th>
                                                <th class="hidden text-right md:table-cell">Price</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($transaction->details as $item)
                                            <tr>
                                                <td class="hidden pb-4 md:table-cell">
                                                    <a href="#">
                                                        <img src="{{ $item->product->foto }}" class="rounded"
                                                            alt="Thumbnail" style="max-width: 7rem">
                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#">
                                                        <p class="mb-2 md:ml-4">{{ $item->product->nama }}</p>

                                                    </a>
                                                </td>
                                                <td>
                                                    <a href="#">
                                                        <p class="mb-2 md:ml-4">{{ $item->qty }}</p>

                                                    </a>
                                                </td>
                                                <td class="hidden text-right md:table-cell">
                                                    <span class="text-sm font-medium lg:text-base">
                                                        Rp. {{ number_format($item->product->harga) }}
                                                    </span>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>Total</td>
                                                <td>Rp. {{number_format($transaction->subtotal)}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div><!--End Table-->
                            </div>
                            <!--End InvoiceBot-->

                        </div>
                        <!--End Invoice-->

                    </div>
                    <!-- End Invoice Holder-->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
