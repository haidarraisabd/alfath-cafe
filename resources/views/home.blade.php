@extends('layouts.main')
@section('content')
<!--admin option section start-->
<section class="section-space bg-white" id="menus">
    <div class="container">
        <div class="col-sm-12 col-xl-6 xl-100">
            <div class="card">
                <div class="card-header">
                    <h5>Our Menus</h5>
                    <span><b>cast</b>, <span class="txt-success">choose</span> and <span
                            class="txt-danger">chill</span></span>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-material nav-primary" id="info-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="info-home-tab" data-toggle="tab" href="#info-home" role="tab"
                                aria-controls="info-home" aria-selected="true">
                                <i class="fa fa-cutlery"></i>Food</a>
                            <div class="material-border"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-info-tab" data-toggle="tab" href="#info-profile" role="tab"
                                aria-controls="info-profile" aria-selected="false"><i class="fa fa-glass"></i>Drink</a>
                            <div class="material-border"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-info-tab" data-toggle="tab" href="#info-contact" role="tab"
                                aria-controls="info-contact" aria-selected="false">
                                <i class="fa fa-spoon"></i>Siders</a>
                            <div class="material-border"></div>
                        </li>
                    </ul>
                    <div class="tab-content" id="info-tabContent">
                        <div class="tab-pane fade show active" id="info-home" role="tabpanel"
                            aria-labelledby="info-home-tab">
                            <div class="row">
                                @foreach ($makanan as $key => $food)
                                <form id="formAdd{{$food->id}}" action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{ $food->id }}" name="id">
                                    <input type="hidden" value="{{ $food->nama }}" name="name">
                                    <input type="hidden" value="{{ $food->harga }}" name="price">
                                    <input type="hidden" value="{{ $food->foto }}"  name="image">
                                    <input type="hidden" value="1" name="quantity">
                                </form>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card" style="height: 400px">
                                        <div class="product-box">
                                            <div class="product-img">
                                                <img src="{{$food->foto}}"
                                                    class="img-fluid" alt="" onerror="this.onerror=null; this.src='{{asset('assets/images/ecommerce/product/01.jpg')}}'" style="width: 200px; height: 250px;"/>
                                                <div class="product-hover">
                                                    <ul>
                                                        <li><a class="addToCart" data-id="{{$food->id}}"><i class="icon-shopping-cart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-details">
                                                <h6>{{$food->nama}}</h6>
                                                <p>{{$food->jenis}}</p>
                                                <div class="product-price">
                                                    Rp. {{number_format($food->harga)}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="info-profile" role="tabpanel" aria-labelledby="profile-info-tab">
                            <div class="row">
                                @foreach ($minuman as $drink)
                                <form id="formAdd{{$drink->id}}" action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{ $drink->id }}" name="id">
                                    <input type="hidden" value="{{ $drink->nama }}" name="name">
                                    <input type="hidden" value="{{ $drink->harga }}" name="price">
                                    <input type="hidden" value="{{ $drink->foto }}"  name="image">
                                    <input type="hidden" value="1" name="quantity">
                                </form>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card" style="height: 400px">
                                        <div class="product-box">
                                            <div class="product-img">
                                                <img src="{{$drink->foto}}"
                                                    class="img-fluid" alt="" onerror="this.onerror=null; this.src='{{asset('assets/images/ecommerce/product/01.jpg')}}'" style="width: 200px; height: 250px;"/>
                                                <div class="product-hover">
                                                    <ul>
                                                        <li><a class="addToCart" data-id="{{$drink->id}}"><i class="icon-shopping-cart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-details">
                                                <h6>{{$drink->nama}}</h6>
                                                <p>{{$drink->jenis}}</p>
                                                <div class="product-price">
                                                    Rp. {{number_format($drink->harga)}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="tab-pane fade" id="info-contact" role="tabpanel" aria-labelledby="contact-info-tab">
                            <div class="row">
                                @foreach ($snack as $sider)
                                <form id="formAdd{{$sider->id}}" action="{{ route('cart.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" value="{{ $sider->id }}" name="id">
                                    <input type="hidden" value="{{ $sider->nama }}" name="name">
                                    <input type="hidden" value="{{ $sider->harga }}" name="price">
                                    <input type="hidden" value="{{ $sider->foto }}"  name="image">
                                    <input type="hidden" value="1" name="quantity">
                                </form>
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card" style="height: 400px">
                                        <div class="product-box">
                                            <div class="product-img">
                                                <img src="{{$sider->foto}}"
                                                    class="img-fluid" alt="" onerror="this.onerror=null; this.src='{{asset('assets/images/ecommerce/product/01.jpg')}}'" style="width: 200px; height: 250px;"/>
                                                <div class="product-hover">
                                                    <ul>
                                                        <li><a class="addToCart" data-id="{{$sider->id}}"><i class="icon-shopping-cart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-details">
                                                <h6>{{$sider->nama}}</h6>
                                                <p>{{$sider->jenis}}</p>
                                                <div class="product-price">
                                                    Rp. {{number_format($sider->harga)}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--admin option section ends-->
@endsection
@section('js')
    <script>
        $(document).on('click', '.addToCart', function(){
            let id = $(this).data('id');
            console.log(id);
            $(`#formAdd${id}`).submit();
        });
    </script>
@endsection
