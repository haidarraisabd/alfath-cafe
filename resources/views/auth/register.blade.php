@extends('layouts.main')
@section('content')
<!-- Loader starts -->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <h4>Have a great day at work today <span>&#x263A;</span></h4>
    </div>
</div>
<!-- Loader ends -->

<!--Page start-->
<div class="page-wrapper">
    <div class="auth-bg">
        <div class="authentication-box">
            <h4 class="text-center">Register</h4>
            <h6 class="text-center">Enter your Username and Password For <a href="{{ route('login') }}">Login</a> or <a
                    href="#">Signup</a></h6>
            <div class="card mt-4 p-4 mb-0">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="form-group">
                        <label for="name" class="col-form-label pt-0">{{ __('Name') }}</label>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                            name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-form-label pt-0" for="email">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label class="col-form-label" for="password">Password</label>
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" name="password" required
                            autocomplete="current-password">
                    </div>
                    <div class="form-group">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm
                            Password') }}</label>
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation"
                            required autocomplete="new-password">
                    </div>
                    <div class="form-group form-row mt-3 mb-0">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="auth-bg-effect">
        <div class="first-effect"></div>
        <div class="second-effect"></div>
        <div class="third-effect"></div>
        <div class="fourth-effect"></div>
    </div>

</div>
<!--Page ends-->
@endsection
