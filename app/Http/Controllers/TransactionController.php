<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\TransactionDetail;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transaction = [];
        try {
            DB::beginTransaction();
            $transaction = Transaction::create([
                'user_id'   => $request->user_id,
                'subtotal'  => $request->subtotal,
                'no_meja'   => $request->no_meja,
                'is_cash'   => $request->is_cash,
                'nominal'   => $request->nominal??0,
            ]);
            $cartItems = Cart::getContent();
            if (count($cartItems) == 0) {
                throw new Exception('Keranjang belanja kosong harap isi terlebih dahulu', 1);
            }
            foreach ($cartItems as $value) {
                $details = DB::table('transaction_details')
                ->insert([
                    'transaction_id'    => $transaction->id,
                    'product_id'        => $value->id,
                    'qty'               => $value->quantity,
                ]);
            }
            Cart::clear();
            DB::commit();
            toast('berhasil checkout', 'success', 'bottom-end');
            return redirect("checkout/$transaction->id")->with('success', 'berhasil checkout');

        } catch (\Throwable $th) {
            //throw $th;
            $transactionDel = Transaction::destroy($transaction->id);
            DB::rollback();
            toast($th->getMessage(), 'error', 'bottom-end');
            return redirect()->back();

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);
        return view('invoice', compact('transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
