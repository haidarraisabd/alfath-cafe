<?php

namespace App\Http\Controllers;

use App\Models\API\Product;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $minuman = Product::get()->whereNotIn('jenis', ['iuran kantor', 'Meeting', 'Lain-lain', 'Makanan', 'Snack', 'Food', 'Topping']);
        $makanan = Product::get()->whereIn('jenis', ['Makanan', 'Food', 'Topping']);
        $snack = Product::get()->whereIn('jenis', ['Snack']);
        // return dd($minuman);
        // return dd($makanan);
        return view('home', compact('makanan', 'minuman', 'snack'));
    }
}
