<?php

namespace App\Models;

use App\Models\API\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_id',
        'product_id',
        'qty'
    ];

    protected $append = ['product'];

    public function getProductAttribute()
    {
        $product = Product::get()->where('id', $this->product_id)->first();
        return $product;
    }
}
