<?php
namespace App\Models\API;

use GuzzleHttp\Client;

class Product {

    protected $url = "https://space.alfathtech.co.id/api/product";

    public static function get()
    {
        $client = new Client();
        $request = $client->get("https://space.alfathtech.co.id/api/produk");
        $response = collect(json_decode($request->getBody())->data);
        return $response;
    }

}
